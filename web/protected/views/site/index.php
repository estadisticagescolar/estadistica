<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;

?>

<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/styles-iview.css'; ?>" />
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/iview.css'; ?>" />
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/skin-4/style.css'; ?>" />

<div class="col-xs-12">
    <div class="row row-fluid">
        
    </div>
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/raphael-min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.easing.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.fullscreen.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/iview.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/slider.js',CClientScript::POS_END); ?>