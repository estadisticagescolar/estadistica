<?php

/**
 * This is the model class for table "reportes.primaria".
 *
 * The followings are the available columns in table 'reportes.primaria':
 * @property integer $id
 * @property string $estado
 * @property string $municipio
 * @property string $parroquia
 * @property string $cod_plantel
 * @property integer $cod_estadistico
 * @property string $fecha_fundacion
 * @property string $nombre_plantel
 * @property string $direccion_plantel
 * @property integer $telefono_plantel
 * @property string $datos_director
 * @property string $correo_plantel
 * @property string $turno_plantel
 * @property integer $secciones_plantel
 * @property integer $secciones_primaria
 * @property integer $secciones_primero
 * @property integer $secciones_segundo
 * @property integer $secciones_tercero
 * @property integer $secciones_cuarto
 * @property integer $secciones_quinto
 * @property integer $secciones_sexto
 * @property integer $matricula_totalplantel
 * @property integer $matricula_totalprimaria
 * @property integer $matricula_totalnacionalidad
 * @property integer $matricula_venezolana
 * @property integer $matricula_extranjera
 * @property integer $matricula_totalgeografico
 * @property integer $matricula_rural
 * @property integer $matricula_urbano
 * @property integer $matricula_totalsexo
 * @property integer $matricula_masculino
 * @property integer $matricula_femenino
 * @property integer $matricula_oficialplantel
 * @property integer $matricula_totaloficial
 * @property integer $matricula_oficialnacional
 * @property integer $matricula_oficialestadal
 * @property integer $matricula_oficialmunicipal
 * @property integer $matricula_oficialautonoma
 * @property integer $matricula_privadaplantel
 * @property integer $matricula_totalprivada
 * @property integer $matricula_privada
 * @property integer $matricula_subvencionadamppe
 * @property integer $matricula_subvencionadaoficial
 * @property integer $matricula_totalgrados
 * @property integer $matricula_primero
 * @property integer $matricula_segundo
 * @property integer $matricula_tercero
 * @property integer $matricula_cuarto
 * @property integer $matricula_quinto
 * @property integer $matricula_sexto
 * @property integer $matricula_totaledad
 * @property integer $matricula_edad5
 * @property integer $matricula_edad6
 * @property integer $matricula_edad7
 * @property integer $matricula_edad8
 * @property integer $matricula_edad9
 * @property integer $matricula_edad10
 * @property integer $matricula_edad11
 * @property integer $matricula_edad12
 * @property integer $matricula_edad13
 * @property integer $matricula_edad14
 * @property integer $matricula_edad15
 * @property integer $matricula_edad16
 * @property integer $matricula_edad17
 * @property integer $matricula_edad18
 * @property integer $matricula_edad19
 * @property integer $matricula_edad20
 * @property integer $matricula_edad21omas
 * @property integer $matricula_indigena
 * @property integer $personal_plantel
 * @property integer $personal_total
 * @property integer $personal_docente
 * @property integer $personal_administrativo
 * @property integer $personal_obrero
 */
class Primaria extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reportes.primaria';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('estado, municipio, parroquia, cod_plantel, cod_estadistico, fecha_fundacion, nombre_plantel, direccion_plantel, telefono_plantel, turno_plantel', 'required'),
			array('cod_estadistico, telefono_plantel, secciones_plantel, secciones_primaria, secciones_primero, secciones_segundo, secciones_tercero, secciones_cuarto, secciones_quinto, secciones_sexto, matricula_totalplantel, matricula_totalprimaria, matricula_totalnacionalidad, matricula_venezolana, matricula_extranjera, matricula_totalgeografico, matricula_rural, matricula_urbano, matricula_totalsexo, matricula_masculino, matricula_femenino, matricula_oficialplantel, matricula_totaloficial, matricula_oficialnacional, matricula_oficialestadal, matricula_oficialmunicipal, matricula_oficialautonoma, matricula_privadaplantel, matricula_totalprivada, matricula_privada, matricula_subvencionadamppe, matricula_subvencionadaoficial, matricula_totalgrados, matricula_primero, matricula_segundo, matricula_tercero, matricula_cuarto, matricula_quinto, matricula_sexto, matricula_totaledad, matricula_edad5, matricula_edad6, matricula_edad7, matricula_edad8, matricula_edad9, matricula_edad10, matricula_edad11, matricula_edad12, matricula_edad13, matricula_edad14, matricula_edad15, matricula_edad16, matricula_edad17, matricula_edad18, matricula_edad19, matricula_edad20, matricula_edad21omas, matricula_indigena, personal_plantel, personal_total, personal_docente, personal_administrativo, personal_obrero', 'numerical', 'integerOnly'=>true),
			array('estado, municipio, parroquia', 'length', 'max'=>65),
			array('cod_plantel, direccion_plantel, datos_director', 'length', 'max'=>100),
			array('fecha_fundacion', 'length', 'max'=>4),
			array('nombre_plantel', 'length', 'max'=>50),
			array('correo_plantel', 'length', 'max'=>40),
			array('turno_plantel', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, estado, municipio, parroquia, cod_plantel, cod_estadistico, fecha_fundacion, nombre_plantel, direccion_plantel, telefono_plantel, datos_director, correo_plantel, turno_plantel, secciones_plantel, secciones_primaria, secciones_primero, secciones_segundo, secciones_tercero, secciones_cuarto, secciones_quinto, secciones_sexto, matricula_totalplantel, matricula_totalprimaria, matricula_totalnacionalidad, matricula_venezolana, matricula_extranjera, matricula_totalgeografico, matricula_rural, matricula_urbano, matricula_totalsexo, matricula_masculino, matricula_femenino, matricula_oficialplantel, matricula_totaloficial, matricula_oficialnacional, matricula_oficialestadal, matricula_oficialmunicipal, matricula_oficialautonoma, matricula_privadaplantel, matricula_totalprivada, matricula_privada, matricula_subvencionadamppe, matricula_subvencionadaoficial, matricula_totalgrados, matricula_primero, matricula_segundo, matricula_tercero, matricula_cuarto, matricula_quinto, matricula_sexto, matricula_totaledad, matricula_edad5, matricula_edad6, matricula_edad7, matricula_edad8, matricula_edad9, matricula_edad10, matricula_edad11, matricula_edad12, matricula_edad13, matricula_edad14, matricula_edad15, matricula_edad16, matricula_edad17, matricula_edad18, matricula_edad19, matricula_edad20, matricula_edad21omas, matricula_indigena, personal_plantel, personal_total, personal_docente, personal_administrativo, personal_obrero', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'estado' => 'Estado',
			'municipio' => 'Municipio',
			'parroquia' => 'Parroquia',
			'cod_plantel' => 'Cod Plantel',
			'cod_estadistico' => 'Cod Estadistico',
			'fecha_fundacion' => 'Fecha Fundacion',
			'nombre_plantel' => 'Nombre Plantel',
			'direccion_plantel' => 'Direccion Plantel',
			'telefono_plantel' => 'Telefono Plantel',
			'datos_director' => 'Datos Director',
			'correo_plantel' => 'Correo Plantel',
			'turno_plantel' => 'Turno Plantel',
			'secciones_plantel' => 'Secciones Plantel',
			'secciones_primaria' => 'Secciones Primaria',
			'secciones_primero' => 'Secciones Primero',
			'secciones_segundo' => 'Secciones Segundo',
			'secciones_tercero' => 'Secciones Tercero',
			'secciones_cuarto' => 'Secciones Cuarto',
			'secciones_quinto' => 'Secciones Quinto',
			'secciones_sexto' => 'Secciones Sexto',
			'matricula_totalplantel' => 'Matricula Totalplantel',
			'matricula_totalprimaria' => 'Matricula Totalprimaria',
			'matricula_totalnacionalidad' => 'Matricula Totalnacionalidad',
			'matricula_venezolana' => 'Matricula Venezolana',
			'matricula_extranjera' => 'Matricula Extranjera',
			'matricula_totalgeografico' => 'Matricula Totalgeografico',
			'matricula_rural' => 'Matricula Rural',
			'matricula_urbano' => 'Matricula Urbano',
			'matricula_totalsexo' => 'Matricula Totalsexo',
			'matricula_masculino' => 'Matricula Masculino',
			'matricula_femenino' => 'Matricula Femenino',
			'matricula_oficialplantel' => 'Matricula Oficialplantel',
			'matricula_totaloficial' => 'Matricula Totaloficial',
			'matricula_oficialnacional' => 'Matricula Oficialnacional',
			'matricula_oficialestadal' => 'Matricula Oficialestadal',
			'matricula_oficialmunicipal' => 'Matricula Oficialmunicipal',
			'matricula_oficialautonoma' => 'Matricula Oficialautonoma',
			'matricula_privadaplantel' => 'Matricula Privadaplantel',
			'matricula_totalprivada' => 'Matricula Totalprivada',
			'matricula_privada' => 'Matricula Privada',
			'matricula_subvencionadamppe' => 'Matricula Subvencionadamppe',
			'matricula_subvencionadaoficial' => 'Matricula Subvencionadaoficial',
			'matricula_totalgrados' => 'Matricula Totalgrados',
			'matricula_primero' => 'Matricula Primero',
			'matricula_segundo' => 'Matricula Segundo',
			'matricula_tercero' => 'Matricula Tercero',
			'matricula_cuarto' => 'Matricula Cuarto',
			'matricula_quinto' => 'Matricula Quinto',
			'matricula_sexto' => 'Matricula Sexto',
			'matricula_totaledad' => 'Matricula Totaledad',
			'matricula_edad5' => 'Matricula Edad5',
			'matricula_edad6' => 'Matricula Edad6',
			'matricula_edad7' => 'Matricula Edad7',
			'matricula_edad8' => 'Matricula Edad8',
			'matricula_edad9' => 'Matricula Edad9',
			'matricula_edad10' => 'Matricula Edad10',
			'matricula_edad11' => 'Matricula Edad11',
			'matricula_edad12' => 'Matricula Edad12',
			'matricula_edad13' => 'Matricula Edad13',
			'matricula_edad14' => 'Matricula Edad14',
			'matricula_edad15' => 'Matricula Edad15',
			'matricula_edad16' => 'Matricula Edad16',
			'matricula_edad17' => 'Matricula Edad17',
			'matricula_edad18' => 'Matricula Edad18',
			'matricula_edad19' => 'Matricula Edad19',
			'matricula_edad20' => 'Matricula Edad20',
			'matricula_edad21omas' => 'Matricula Edad21omas',
			'matricula_indigena' => 'Matricula Indigena',
			'personal_plantel' => 'Personal Plantel',
			'personal_total' => 'Personal Total',
			'personal_docente' => 'Personal Docente',
			'personal_administrativo' => 'Personal Administrativo',
			'personal_obrero' => 'Personal Obrero',
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		//$criteria->compare('estado',$this->estado,true);
		$criteria->addSearchCondition('estado', '%' . $this->estado . '%', false, 'AND', 'ILIKE');
		//$criteria->compare('municipio',$this->municipio,true);
		$criteria->addSearchCondition('municipio', '%' . $this->municipio . '%', false, 'AND', 'ILIKE');
		$criteria->compare('parroquia',$this->parroquia,true);
		//$criteria->compare('cod_plantel',$this->cod_plantel,true);
		$criteria->addSearchCondition('cod_plantel', '%' . $this->cod_plantel . '%', false, 'AND', 'ILIKE');
		
		$criteria->compare('cod_estadistico',$this->cod_estadistico);

		$criteria->compare('fecha_fundacion',$this->fecha_fundacion,true);
		//$criteria->compare('nombre_plantel',$this->nombre_plantel,true);
		$criteria->addSearchCondition('nombre_plantel', '%' . $this->nombre_plantel . '%', false, 'AND', 'ILIKE');
		$criteria->compare('direccion_plantel',$this->direccion_plantel,true);
		$criteria->compare('telefono_plantel',$this->telefono_plantel);
		$criteria->compare('datos_director',$this->datos_director,true);
		$criteria->compare('correo_plantel',$this->correo_plantel,true);
		$criteria->compare('turno_plantel',$this->turno_plantel,true);
		$criteria->compare('secciones_plantel',$this->secciones_plantel);
		$criteria->compare('secciones_primaria',$this->secciones_primaria);
		$criteria->compare('secciones_primero',$this->secciones_primero);
		$criteria->compare('secciones_segundo',$this->secciones_segundo);
		$criteria->compare('secciones_tercero',$this->secciones_tercero);
		$criteria->compare('secciones_cuarto',$this->secciones_cuarto);
		$criteria->compare('secciones_quinto',$this->secciones_quinto);
		$criteria->compare('secciones_sexto',$this->secciones_sexto);
		$criteria->compare('matricula_totalplantel',$this->matricula_totalplantel);
		$criteria->compare('matricula_totalprimaria',$this->matricula_totalprimaria);
		$criteria->compare('matricula_totalnacionalidad',$this->matricula_totalnacionalidad);
		$criteria->compare('matricula_venezolana',$this->matricula_venezolana);
		$criteria->compare('matricula_extranjera',$this->matricula_extranjera);
		$criteria->compare('matricula_totalgeografico',$this->matricula_totalgeografico);
		$criteria->compare('matricula_rural',$this->matricula_rural);
		$criteria->compare('matricula_urbano',$this->matricula_urbano);
		$criteria->compare('matricula_totalsexo',$this->matricula_totalsexo);
		$criteria->compare('matricula_masculino',$this->matricula_masculino);
		$criteria->compare('matricula_femenino',$this->matricula_femenino);
		$criteria->compare('matricula_oficialplantel',$this->matricula_oficialplantel);
		$criteria->compare('matricula_totaloficial',$this->matricula_totaloficial);
		$criteria->compare('matricula_oficialnacional',$this->matricula_oficialnacional);
		$criteria->compare('matricula_oficialestadal',$this->matricula_oficialestadal);
		$criteria->compare('matricula_oficialmunicipal',$this->matricula_oficialmunicipal);
		$criteria->compare('matricula_oficialautonoma',$this->matricula_oficialautonoma);
		$criteria->compare('matricula_privadaplantel',$this->matricula_privadaplantel);
		$criteria->compare('matricula_totalprivada',$this->matricula_totalprivada);
		$criteria->compare('matricula_privada',$this->matricula_privada);
		$criteria->compare('matricula_subvencionadamppe',$this->matricula_subvencionadamppe);
		$criteria->compare('matricula_subvencionadaoficial',$this->matricula_subvencionadaoficial);
		$criteria->compare('matricula_totalgrados',$this->matricula_totalgrados);
		$criteria->compare('matricula_primero',$this->matricula_primero);
		$criteria->compare('matricula_segundo',$this->matricula_segundo);
		$criteria->compare('matricula_tercero',$this->matricula_tercero);
		$criteria->compare('matricula_cuarto',$this->matricula_cuarto);
		$criteria->compare('matricula_quinto',$this->matricula_quinto);
		$criteria->compare('matricula_sexto',$this->matricula_sexto);
		$criteria->compare('matricula_totaledad',$this->matricula_totaledad);
		$criteria->compare('matricula_edad5',$this->matricula_edad5);
		$criteria->compare('matricula_edad6',$this->matricula_edad6);
		$criteria->compare('matricula_edad7',$this->matricula_edad7);
		$criteria->compare('matricula_edad8',$this->matricula_edad8);
		$criteria->compare('matricula_edad9',$this->matricula_edad9);
		$criteria->compare('matricula_edad10',$this->matricula_edad10);
		$criteria->compare('matricula_edad11',$this->matricula_edad11);
		$criteria->compare('matricula_edad12',$this->matricula_edad12);
		$criteria->compare('matricula_edad13',$this->matricula_edad13);
		$criteria->compare('matricula_edad14',$this->matricula_edad14);
		$criteria->compare('matricula_edad15',$this->matricula_edad15);
		$criteria->compare('matricula_edad16',$this->matricula_edad16);
		$criteria->compare('matricula_edad17',$this->matricula_edad17);
		$criteria->compare('matricula_edad18',$this->matricula_edad18);
		$criteria->compare('matricula_edad19',$this->matricula_edad19);
		$criteria->compare('matricula_edad20',$this->matricula_edad20);
		$criteria->compare('matricula_edad21omas',$this->matricula_edad21omas);
		$criteria->compare('matricula_indigena',$this->matricula_indigena);
		$criteria->compare('personal_plantel',$this->personal_plantel);
		$criteria->compare('personal_total',$this->personal_total);
		$criteria->compare('personal_docente',$this->personal_docente);
		$criteria->compare('personal_administrativo',$this->personal_administrativo);
		$criteria->compare('personal_obrero',$this->personal_obrero);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Primaria the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
