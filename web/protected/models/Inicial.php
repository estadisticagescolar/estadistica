<?php

/**
 * This is the model class for table "reportes.inicial".
 *
 * The followings are the available columns in table 'reportes.inicial':
 * @property integer $id
 * @property string $estado
 * @property string $municipio
 * @property string $parroquia
 * @property string $cod_plantel
 * @property integer $cod_estadistico
 * @property string $fecha_fundacion
 * @property string $nombre_plantel
 * @property string $direccion_plantel
 * @property integer $telefono_plantel
 * @property string $datos_director
 * @property string $correo_plantel
 * @property string $turno_plantel
 * @property integer $secciones_plantel
 * @property integer $matricula_totalplantel
 * @property integer $matricula_venezolana
 * @property integer $matricula_extranjera
 * @property string $matricula_nombregeografico
 * @property integer $matricula_totalgeografico
 * @property integer $matricula_masculino
 * @property integer $matricula_femenino
 * @property string $matricula_dependencianombre
 * @property integer $matricula_dependenciaplantel
 * @property integer $matricula_etapamaternaltotal
 * @property integer $matricula_etapamaternaledadmenosde1
 * @property integer $matricula_etapamaternaledad1
 * @property integer $matricula_etapamaternaledad2
 * @property integer $matricula_etapapreescolartotal
 * @property integer $matricula_etapapreescolaredad3
 * @property integer $matricula_etapapreescolaredad4
 * @property integer $matricula_etapapreescolaredad5
 * @property integer $matricula_etapapreescolaredad6
 * @property integer $matricula_indigena
 */
class Inicial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reportes.inicial';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('estado, municipio, parroquia, cod_plantel, cod_estadistico, fecha_fundacion, nombre_plantel, direccion_plantel, telefono_plantel, turno_plantel', 'required'),
			array('cod_estadistico, telefono_plantel, secciones_plantel, matricula_totalplantel, matricula_venezolana, matricula_extranjera, matricula_totalgeografico, matricula_masculino, matricula_femenino, matricula_dependenciaplantel, matricula_etapamaternaltotal, matricula_etapamaternaledadmenosde1, matricula_etapamaternaledad1, matricula_etapamaternaledad2, matricula_etapapreescolartotal, matricula_etapapreescolaredad3, matricula_etapapreescolaredad4, matricula_etapapreescolaredad5, matricula_etapapreescolaredad6, matricula_indigena', 'numerical', 'integerOnly'=>true),
			array('estado, municipio, parroquia', 'length', 'max'=>65),
			array('cod_plantel', 'length', 'max'=>50),
			array('fecha_fundacion', 'length', 'max'=>4),
			array('nombre_plantel', 'length', 'max'=>60),
			array('direccion_plantel, datos_director', 'length', 'max'=>100),
			array('correo_plantel', 'length', 'max'=>40),
			array('turno_plantel, matricula_nombregeografico', 'length', 'max'=>10),
			array('matricula_dependencianombre', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, estado, municipio, parroquia, cod_plantel, cod_estadistico, fecha_fundacion, nombre_plantel, direccion_plantel, telefono_plantel, datos_director, correo_plantel, turno_plantel, secciones_plantel, matricula_totalplantel, matricula_venezolana, matricula_extranjera, matricula_nombregeografico, matricula_totalgeografico, matricula_masculino, matricula_femenino, matricula_dependencianombre, matricula_dependenciaplantel, matricula_etapamaternaltotal, matricula_etapamaternaledadmenosde1, matricula_etapamaternaledad1, matricula_etapamaternaledad2, matricula_etapapreescolartotal, matricula_etapapreescolaredad3, matricula_etapapreescolaredad4, matricula_etapapreescolaredad5, matricula_etapapreescolaredad6, matricula_indigena', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'estado' => 'Estado',
			'municipio' => 'Municipio',
			'parroquia' => 'Parroquia',
			'cod_plantel' => 'Cod Plantel',
			'cod_estadistico' => 'Cod Estadistico',
			'fecha_fundacion' => 'Fecha Fundacion',
			'nombre_plantel' => 'Nombre Plantel',
			'direccion_plantel' => 'Direccion Plantel',
			'telefono_plantel' => 'Telefono Plantel',
			'datos_director' => 'Datos Director',
			'correo_plantel' => 'Correo Plantel',
			'turno_plantel' => 'Turno Plantel',
			'secciones_plantel' => 'Secciones Plantel',
			'matricula_totalplantel' => 'Matricula Totalplantel',
			'matricula_venezolana' => 'Matricula Venezolana',
			'matricula_extranjera' => 'Matricula Extranjera',
			'matricula_nombregeografico' => 'Matricula Nombregeografico',
			'matricula_totalgeografico' => 'Matricula Totalgeografico',
			'matricula_masculino' => 'Matricula Masculino',
			'matricula_femenino' => 'Matricula Femenino',
			'matricula_dependencianombre' => 'Matricula Dependencianombre',
			'matricula_dependenciaplantel' => 'Matricula Dependenciaplantel',
			'matricula_etapamaternaltotal' => 'Matricula Etapamaternaltotal',
			'matricula_etapamaternaledadmenosde1' => 'Matricula Etapamaternaledadmenosde1',
			'matricula_etapamaternaledad1' => 'Matricula Etapamaternaledad1',
			'matricula_etapamaternaledad2' => 'Matricula Etapamaternaledad2',
			'matricula_etapapreescolartotal' => 'Matricula Etapapreescolartotal',
			'matricula_etapapreescolaredad3' => 'Matricula Etapapreescolaredad3',
			'matricula_etapapreescolaredad4' => 'Matricula Etapapreescolaredad4',
			'matricula_etapapreescolaredad5' => 'Matricula Etapapreescolaredad5',
			'matricula_etapapreescolaredad6' => 'Matricula Etapapreescolaredad6',
			'matricula_indigena' => 'Matricula Indigena',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('municipio',$this->municipio,true);
		$criteria->compare('parroquia',$this->parroquia,true);
		$criteria->compare('cod_plantel',$this->cod_plantel,true);
		$criteria->compare('cod_estadistico',$this->cod_estadistico);
		$criteria->compare('fecha_fundacion',$this->fecha_fundacion,true);
		$criteria->compare('nombre_plantel',$this->nombre_plantel,true);
		$criteria->compare('direccion_plantel',$this->direccion_plantel,true);
		$criteria->compare('telefono_plantel',$this->telefono_plantel);
		$criteria->compare('datos_director',$this->datos_director,true);
		$criteria->compare('correo_plantel',$this->correo_plantel,true);
		$criteria->compare('turno_plantel',$this->turno_plantel,true);
		$criteria->compare('secciones_plantel',$this->secciones_plantel);
		$criteria->compare('matricula_totalplantel',$this->matricula_totalplantel);
		$criteria->compare('matricula_venezolana',$this->matricula_venezolana);
		$criteria->compare('matricula_extranjera',$this->matricula_extranjera);
		$criteria->compare('matricula_nombregeografico',$this->matricula_nombregeografico,true);
		$criteria->compare('matricula_totalgeografico',$this->matricula_totalgeografico);
		$criteria->compare('matricula_masculino',$this->matricula_masculino);
		$criteria->compare('matricula_femenino',$this->matricula_femenino);
		$criteria->compare('matricula_dependencianombre',$this->matricula_dependencianombre,true);
		$criteria->compare('matricula_dependenciaplantel',$this->matricula_dependenciaplantel);
		$criteria->compare('matricula_etapamaternaltotal',$this->matricula_etapamaternaltotal);
		$criteria->compare('matricula_etapamaternaledadmenosde1',$this->matricula_etapamaternaledadmenosde1);
		$criteria->compare('matricula_etapamaternaledad1',$this->matricula_etapamaternaledad1);
		$criteria->compare('matricula_etapamaternaledad2',$this->matricula_etapamaternaledad2);
		$criteria->compare('matricula_etapapreescolartotal',$this->matricula_etapapreescolartotal);
		$criteria->compare('matricula_etapapreescolaredad3',$this->matricula_etapapreescolaredad3);
		$criteria->compare('matricula_etapapreescolaredad4',$this->matricula_etapapreescolaredad4);
		$criteria->compare('matricula_etapapreescolaredad5',$this->matricula_etapapreescolaredad5);
		$criteria->compare('matricula_etapapreescolaredad6',$this->matricula_etapapreescolaredad6);
		$criteria->compare('matricula_indigena',$this->matricula_indigena);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inicial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public static function getPdfGeneral(){
            $sql="select * from reportes.inicial";
            $consulta = Yii::app()->db->createCommand($sql);
            $resultado = $consulta->queryAll();
            return $resultado;
        }
               
        
}
