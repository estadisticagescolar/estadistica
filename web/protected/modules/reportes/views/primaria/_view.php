<?php
/* @var $this PrimariaController */
/* @var $model Primaria */

$this->breadcrumbs=array(
	'Primaria'=>array('lista'),
);
?>
<div class="tabbable">

    <ul class="nav nav-tabs">
        <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="datosGenerales">
            <div class="form">

        <div id="div-datos-generales">

            <div class="widget-box">

                <div class="widget-header">
                    <h5>Datos Generales</h5>

                    <div class="widget-toolbar">
                        <a data-action="collapse" href="#">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-body-inner">
                        <div class="widget-main">
                            <div class="widget-main form">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'estado'); ?>
                                            <?php echo $form->textField($model,'estado',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'municipio'); ?>
                                            <?php echo $form->textField($model,'municipio',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'parroquia'); ?>
                                            <?php echo $form->textField($model,'parroquia',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'cod_plantel'); ?>
                                            <?php echo $form->textField($model,'cod_plantel',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'cod_estadistico'); ?>
                                            <?php echo $form->textField($model,'cod_estadistico', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_fundacion'); ?>
                                            <?php echo $form->textField($model,'fecha_fundacion',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'nombre_plantel'); ?>
                                            <?php echo $form->textField($model,'nombre_plantel',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'direccion_plantel'); ?>
                                            <?php echo $form->textField($model,'direccion_plantel',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'telefono_plantel'); ?>
                                            <?php echo $form->textField($model,'telefono_plantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'datos_director'); ?>
                                            <?php echo $form->textField($model,'datos_director',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'correo_plantel'); ?>
                                            <?php echo $form->textField($model,'correo_plantel',array('size'=>40, 'maxlength'=>40, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'turno_plantel'); ?>
                                            <?php echo $form->textField($model,'turno_plantel',array('size'=>10, 'maxlength'=>10, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'secciones_plantel'); ?>
                                            <?php echo $form->textField($model,'secciones_plantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'secciones_primaria'); ?>
                                            <?php echo $form->textField($model,'secciones_primaria', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'secciones_primero'); ?>
                                            <?php echo $form->textField($model,'secciones_primero', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'secciones_segundo'); ?>
                                            <?php echo $form->textField($model,'secciones_segundo', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'secciones_tercero'); ?>
                                            <?php echo $form->textField($model,'secciones_tercero', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'secciones_cuarto'); ?>
                                            <?php echo $form->textField($model,'secciones_cuarto', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'secciones_quinto'); ?>
                                            <?php echo $form->textField($model,'secciones_quinto', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'secciones_sexto'); ?>
                                            <?php echo $form->textField($model,'secciones_sexto', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totalplantel'); ?>
                                            <?php echo $form->textField($model,'matricula_totalplantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totalprimaria'); ?>
                                            <?php echo $form->textField($model,'matricula_totalprimaria', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totalnacionalidad'); ?>
                                            <?php echo $form->textField($model,'matricula_totalnacionalidad', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_venezolana'); ?>
                                            <?php echo $form->textField($model,'matricula_venezolana', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_extranjera'); ?>
                                            <?php echo $form->textField($model,'matricula_extranjera', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totalgeografico'); ?>
                                            <?php echo $form->textField($model,'matricula_totalgeografico', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_rural'); ?>
                                            <?php echo $form->textField($model,'matricula_rural', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_urbano'); ?>
                                            <?php echo $form->textField($model,'matricula_urbano', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totalsexo'); ?>
                                            <?php echo $form->textField($model,'matricula_totalsexo', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_masculino'); ?>
                                            <?php echo $form->textField($model,'matricula_masculino', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_femenino'); ?>
                                            <?php echo $form->textField($model,'matricula_femenino', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_oficialplantel'); ?>
                                            <?php echo $form->textField($model,'matricula_oficialplantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totaloficial'); ?>
                                            <?php echo $form->textField($model,'matricula_totaloficial', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_oficialnacional'); ?>
                                            <?php echo $form->textField($model,'matricula_oficialnacional', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_oficialestadal'); ?>
                                            <?php echo $form->textField($model,'matricula_oficialestadal', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_oficialmunicipal'); ?>
                                            <?php echo $form->textField($model,'matricula_oficialmunicipal', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_oficialautonoma'); ?>
                                            <?php echo $form->textField($model,'matricula_oficialautonoma', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_privadaplantel'); ?>
                                            <?php echo $form->textField($model,'matricula_privadaplantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totalprivada'); ?>
                                            <?php echo $form->textField($model,'matricula_totalprivada', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_privada'); ?>
                                            <?php echo $form->textField($model,'matricula_privada', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_subvencionadamppe'); ?>
                                            <?php echo $form->textField($model,'matricula_subvencionadamppe', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_subvencionadaoficial'); ?>
                                            <?php echo $form->textField($model,'matricula_subvencionadaoficial', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totalgrados'); ?>
                                            <?php echo $form->textField($model,'matricula_totalgrados', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_primero'); ?>
                                            <?php echo $form->textField($model,'matricula_primero', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_segundo'); ?>
                                            <?php echo $form->textField($model,'matricula_segundo', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_tercero'); ?>
                                            <?php echo $form->textField($model,'matricula_tercero', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_cuarto'); ?>
                                            <?php echo $form->textField($model,'matricula_cuarto', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_quinto'); ?>
                                            <?php echo $form->textField($model,'matricula_quinto', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_sexto'); ?>
                                            <?php echo $form->textField($model,'matricula_sexto', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_totaledad'); ?>
                                            <?php echo $form->textField($model,'matricula_totaledad', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad5'); ?>
                                            <?php echo $form->textField($model,'matricula_edad5', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad6'); ?>
                                            <?php echo $form->textField($model,'matricula_edad6', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad7'); ?>
                                            <?php echo $form->textField($model,'matricula_edad7', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad8'); ?>
                                            <?php echo $form->textField($model,'matricula_edad8', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad9'); ?>
                                            <?php echo $form->textField($model,'matricula_edad9', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad10'); ?>
                                            <?php echo $form->textField($model,'matricula_edad10', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad11'); ?>
                                            <?php echo $form->textField($model,'matricula_edad11', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad12'); ?>
                                            <?php echo $form->textField($model,'matricula_edad12', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad13'); ?>
                                            <?php echo $form->textField($model,'matricula_edad13', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad14'); ?>
                                            <?php echo $form->textField($model,'matricula_edad14', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad15'); ?>
                                            <?php echo $form->textField($model,'matricula_edad15', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad16'); ?>
                                            <?php echo $form->textField($model,'matricula_edad16', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad17'); ?>
                                            <?php echo $form->textField($model,'matricula_edad17', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad18'); ?>
                                            <?php echo $form->textField($model,'matricula_edad18', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad19'); ?>
                                            <?php echo $form->textField($model,'matricula_edad19', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad20'); ?>
                                            <?php echo $form->textField($model,'matricula_edad20', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_edad21omas'); ?>
                                            <?php echo $form->textField($model,'matricula_edad21omas', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'matricula_indigena'); ?>
                                            <?php echo $form->textField($model,'matricula_indigena', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'personal_plantel'); ?>
                                            <?php echo $form->textField($model,'personal_plantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'personal_total'); ?>
                                            <?php echo $form->textField($model,'personal_total', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'personal_docente'); ?>
                                            <?php echo $form->textField($model,'personal_docente', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'personal_administrativo'); ?>
                                            <?php echo $form->textField($model,'personal_administrativo', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'personal_obrero'); ?>
                                            <?php echo $form->textField($model,'personal_obrero', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div><!-- form -->
        </div>
    </div>
</div>
