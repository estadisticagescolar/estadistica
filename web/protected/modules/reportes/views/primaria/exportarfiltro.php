<?php
/* SELECT 
  primaria.estado, 
  primaria.municipio, 
  primaria.parroquia, 
  primaria.cod_plantel, 
  primaria.fecha_fundacion, 
  primaria.cod_estadistico, 
  primaria.nombre_plantel, 
  primaria.telefono_plantel, 
  primaria.direccion_plantel, 
  primaria.datos_director, 
  primaria.correo_plantel, 
  primaria.turno_plantel, 
  primaria.secciones_primaria, 
  primaria.secciones_plantel, 
  primaria.secciones_primero, 
  primaria.secciones_tercero, 
  primaria.secciones_segundo, 
  primaria.secciones_cuarto, 
  primaria.secciones_quinto, 
  primaria.secciones_sexto, 
  primaria.matricula_totalplantel, 
  primaria.matricula_totalprimaria, 
  primaria.matricula_totalnacionalidad, 
  primaria.matricula_venezolana, 
  primaria.matricula_extranjera, 
  primaria.matricula_totalgeografico, 
  primaria.matricula_rural, 
  primaria.matricula_urbano, 
  primaria.matricula_totalsexo, 
  primaria.matricula_masculino, 
  primaria.matricula_femenino, 
  primaria.matricula_oficialplantel, 
  primaria.matricula_oficialnacional, 
  primaria.matricula_totaloficial, 
  primaria.matricula_oficialestadal, 
  primaria.matricula_oficialmunicipal, 
  primaria.matricula_oficialautonoma, 
  primaria.matricula_privadaplantel, 
  primaria.matricula_totalprivada, 
  primaria.matricula_privada, 
  primaria.matricula_subvencionadamppe, 
  primaria.matricula_totalgrados, 
  primaria.matricula_subvencionadaoficial, 
  primaria.matricula_primero, 
  primaria.matricula_tercero, 
  primaria.matricula_segundo, 
  primaria.matricula_cuarto, 
  primaria.matricula_quinto, 
  primaria.matricula_sexto, 
  primaria.matricula_totaledad, 
  primaria.matricula_edad5
FROM 
  reportes.primaria;*/
?>
<table style="font-size:11px; width:800px; padding: 5px;">
<tr>
<th colspan="12" style="background-color:gray;color:#fff">DIRECTORIO DE EDUCACIÓN PRIMARIA</th>
</tr>
<tr>
<th>Entidad Federal</th>
<td>?</td>
<th>Municipio</th>
<td>?</td>
<th>Parroquia</th>
<td>?</td>
<th>Código Plantel</th>
<td>?</td>
<th>Código Estadístico</th>
<td>?</td>
</tr>
<tr>
<th>Fecha de Fundación del Plantel</th>
<td>?</td>
<th>Nombre del Plantel</th>
<td>?</td>
<th>Dirección del Plantel</th>
<td>?</td>
<th>Teléfono del Plantel</th>
<td>?</td>
<th>Nombre Apellido del Director</th>
<td>?</td>
</tr>
<tr>
<th>Teléfono del Director</th>
<td>?</td>
</tr>
</table>
<table style="font-size:11px; width:800px; padding: 5px;">
<tr>
<th colspan="12" style="background-color:gray;color:#fff">SECCIONES</th>
<tr>
<tr>
<th colspan="12" style="background-color:gray;color:#fff">GRADO DE ESTUDIO</th>
<tr>
<tr>
<th>Primero</th>
<td>?</td>
<th>Segundo</th>
<td>?</td>
<th>Tercero</th>
<td>?</td>
<th>Cuarto</th>
<td>?</td>
<th>Quinto</th>
<td>?</td>
<th>Sexto</th>
<td>?</td>
</tr>
</table>
<table style="font-size:11px; width:800px; padding: 5px;">
<tr>
<th colspan="5" style="background-color:gray;color:#fff">MATRÍCULA</th>
</tr>

<tr>
<th style="background-color:gray;color:#fff">Nacionalidad</th>
<th>Venezolana</th>
<td><center>?<center></td>
<th>Extranjera</th>
<td><center>?<center></td>
</tr>

<tr>
<th style="background-color:gray;color:#fff">Medio geográfico</th>
<th>Urbano</th>
<td><center>?<center></td>
<th>Rural</th>
<td><center>?<center></td>
</tr>

<tr>
<th style="background-color:gray;color:#fff">Sexo</th>
<th>Masculino</th>
<td><center>?<center></td>
<th>Femenino</th>
<td><center>?<center></td>
</tr>


<tr>
<th rowspan="3" style="background-color:gray;color:#fff">Dependencia Oficial</th>
<th>Total</th>
<td><center>?<center></td>
<th>Nacional</th>
<td><center>?<center></td>
</tr>
<tr>
<th>Estadal</th>
<td><center>?<center></td>
<th>Municipal</th>
<td><center>?<center></td>
</tr>
<tr>
<th>Autónoma</th>
<td><center>?<center></td>
</tr>

<tr>
<th rowspan="2" style="background-color:gray;color:#fff">Dependencia Privada</th>
<th>Total</th>
<td><center>?<center></td>
<th>Privada</th>
<td><center>?<center></td>
</tr>
<tr>
<th>Privada Subvencionada MPPE</th>
<td><center>?<center></td>
<th>Privada Subvencionada Oficial</th>
<td><center>?<center></td>
</tr>

<tr>
<th rowspan="3" style="background-color:gray;color:#fff">Grados de Estudios</th>
<th>Primero</th>
<td><center>?<center></td>
<th>Segundo</th>
<td><center>?<center></td>
</tr>
<tr>
<th>Tercero</th>
<td><center>?<center></td>
<th>Cuarto</th>
<td><center>?<center></td>
</tr>
<tr>
<th>Quinto</th>
<td><center>?<center></td>
<th>Sexto</th>
<td><center>?<center></td>
</tr>

<tr>
<th rowspan="9" style="background-color:gray;color:#fff">Edad</th>
<th>5</th>
<td><center>?<center></td>
<th>6</th>
<td><center>?<center></td>
</tr>
<tr>
<th>7</th>
<td><center>?<center></td>
<th>8</th>
<td><center>?<center></td>
</tr>
<tr>
<th>9</th>
<td><center>?<center></td>
<th>10</th>
<td><center>?<center></td>
</tr>
<tr>
<th>11</th>
<td><center>?<center></td>
<th>12</th>
<td><center>?<center></td>
</tr>
<tr>
<th>13</th>
<td><center>?<center></td>
<th>14</th>
<td><center>?<center></td>
</tr>
<tr>
<th>15</th>
<td><center>?<center></td>
<th>16</th>
<td><center>?<center></td>
</tr>
<tr>
<th>17</th>
<td><center>?<center></td>
<th>18</th>
<td><center>?<center></td>
</tr>
<tr>
<th>19</th>
<td><center>?<center></td>
<th>20</th>
<td><center>?<center></td>
</tr>
<tr>
<th>21 o más</th>
<td><center>?<center></td>
</tr>


<tr>
<th colspan="4" style="background-color:gray;color:#fff">Matrícula Indígena Atendida</th>
<td><center>?<center></td>
</tr>


<tr>
<th colspan="4" style="background-color:gray;color:#fff">Total General</th>
<td><center>?<center></td>
</tr>

</table>

<table style="font-size:11px; width:800px; padding: 5px;">
<tr>
<th colspan="2" style="background-color:gray;color:#fff">Docente</th>
</tr>
<tr>
<th style="background-color:gray;color:#fff">Administrativo</th>
<td><center>?<center></td>
</tr>
<tr>
<th style="background-color:gray;color:#fff">Obrero</th>
<td><center>?<center></td>
</tr>
<tr>
<th style="background-color:gray;color:#fff">Total</th>
<td><center>?<center></td>
</tr>
</table>
