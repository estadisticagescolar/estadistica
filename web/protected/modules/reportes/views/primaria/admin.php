<?php

/* @var $this PrimariaController */
/* @var $model Primaria */

$this->breadcrumbs=array(
	'Primaria'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Primaria';

?>
<div class="widget-box">
    <div class="widget-header">
        <h4>Lista de Primaria</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Primaria.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/reportes/primaria/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Primaria                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'primaria-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>estado</center>',
            'name' => 'estado',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[estado]', $model->estado, array('title' => '',)),
        ),
        array(
            'header' => '<center>municipio</center>',
            'name' => 'municipio',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[municipio]', $model->municipio, array('title' => '',)),
        ),
        array(
            'header' => '<center>parroquia</center>',
            'name' => 'parroquia',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[parroquia]', $model->parroquia, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_plantel</center>',
            'name' => 'cod_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[cod_plantel]', $model->cod_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_estadistico</center>',
            'name' => 'cod_estadistico',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[cod_estadistico]', $model->cod_estadistico, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>fecha_fundacion</center>',
            'name' => 'fecha_fundacion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[fecha_fundacion]', $model->fecha_fundacion, array('title' => '',)),
        ),
        array(
            'header' => '<center>nombre_plantel</center>',
            'name' => 'nombre_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[nombre_plantel]', $model->nombre_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>direccion_plantel</center>',
            'name' => 'direccion_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[direccion_plantel]', $model->direccion_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>telefono_plantel</center>',
            'name' => 'telefono_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[telefono_plantel]', $model->telefono_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>datos_director</center>',
            'name' => 'datos_director',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[datos_director]', $model->datos_director, array('title' => '',)),
        ),
        array(
            'header' => '<center>correo_plantel</center>',
            'name' => 'correo_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[correo_plantel]', $model->correo_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>turno_plantel</center>',
            'name' => 'turno_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[turno_plantel]', $model->turno_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_plantel</center>',
            'name' => 'secciones_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[secciones_plantel]', $model->secciones_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_primaria</center>',
            'name' => 'secciones_primaria',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[secciones_primaria]', $model->secciones_primaria, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_primero</center>',
            'name' => 'secciones_primero',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[secciones_primero]', $model->secciones_primero, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_segundo</center>',
            'name' => 'secciones_segundo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[secciones_segundo]', $model->secciones_segundo, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_tercero</center>',
            'name' => 'secciones_tercero',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[secciones_tercero]', $model->secciones_tercero, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_cuarto</center>',
            'name' => 'secciones_cuarto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[secciones_cuarto]', $model->secciones_cuarto, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_quinto</center>',
            'name' => 'secciones_quinto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[secciones_quinto]', $model->secciones_quinto, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_sexto</center>',
            'name' => 'secciones_sexto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[secciones_sexto]', $model->secciones_sexto, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalplantel</center>',
            'name' => 'matricula_totalplantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totalplantel]', $model->matricula_totalplantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalprimaria</center>',
            'name' => 'matricula_totalprimaria',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totalprimaria]', $model->matricula_totalprimaria, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalnacionalidad</center>',
            'name' => 'matricula_totalnacionalidad',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totalnacionalidad]', $model->matricula_totalnacionalidad, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_venezolana</center>',
            'name' => 'matricula_venezolana',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_venezolana]', $model->matricula_venezolana, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_extranjera</center>',
            'name' => 'matricula_extranjera',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_extranjera]', $model->matricula_extranjera, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalgeografico</center>',
            'name' => 'matricula_totalgeografico',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totalgeografico]', $model->matricula_totalgeografico, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_rural</center>',
            'name' => 'matricula_rural',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_rural]', $model->matricula_rural, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_urbano</center>',
            'name' => 'matricula_urbano',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_urbano]', $model->matricula_urbano, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalsexo</center>',
            'name' => 'matricula_totalsexo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totalsexo]', $model->matricula_totalsexo, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_masculino</center>',
            'name' => 'matricula_masculino',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_masculino]', $model->matricula_masculino, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_femenino</center>',
            'name' => 'matricula_femenino',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_femenino]', $model->matricula_femenino, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_oficialplantel</center>',
            'name' => 'matricula_oficialplantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_oficialplantel]', $model->matricula_oficialplantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totaloficial</center>',
            'name' => 'matricula_totaloficial',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totaloficial]', $model->matricula_totaloficial, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_oficialnacional</center>',
            'name' => 'matricula_oficialnacional',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_oficialnacional]', $model->matricula_oficialnacional, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_oficialestadal</center>',
            'name' => 'matricula_oficialestadal',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_oficialestadal]', $model->matricula_oficialestadal, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_oficialmunicipal</center>',
            'name' => 'matricula_oficialmunicipal',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_oficialmunicipal]', $model->matricula_oficialmunicipal, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_oficialautonoma</center>',
            'name' => 'matricula_oficialautonoma',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_oficialautonoma]', $model->matricula_oficialautonoma, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_privadaplantel</center>',
            'name' => 'matricula_privadaplantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_privadaplantel]', $model->matricula_privadaplantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalprivada</center>',
            'name' => 'matricula_totalprivada',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totalprivada]', $model->matricula_totalprivada, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_privada</center>',
            'name' => 'matricula_privada',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_privada]', $model->matricula_privada, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_subvencionadamppe</center>',
            'name' => 'matricula_subvencionadamppe',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_subvencionadamppe]', $model->matricula_subvencionadamppe, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_subvencionadaoficial</center>',
            'name' => 'matricula_subvencionadaoficial',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_subvencionadaoficial]', $model->matricula_subvencionadaoficial, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalgrados</center>',
            'name' => 'matricula_totalgrados',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totalgrados]', $model->matricula_totalgrados, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_primero</center>',
            'name' => 'matricula_primero',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_primero]', $model->matricula_primero, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_segundo</center>',
            'name' => 'matricula_segundo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_segundo]', $model->matricula_segundo, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_tercero</center>',
            'name' => 'matricula_tercero',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_tercero]', $model->matricula_tercero, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_cuarto</center>',
            'name' => 'matricula_cuarto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_cuarto]', $model->matricula_cuarto, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_quinto</center>',
            'name' => 'matricula_quinto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_quinto]', $model->matricula_quinto, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_sexto</center>',
            'name' => 'matricula_sexto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_sexto]', $model->matricula_sexto, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totaledad</center>',
            'name' => 'matricula_totaledad',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_totaledad]', $model->matricula_totaledad, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad5</center>',
            'name' => 'matricula_edad5',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad5]', $model->matricula_edad5, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad6</center>',
            'name' => 'matricula_edad6',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad6]', $model->matricula_edad6, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad7</center>',
            'name' => 'matricula_edad7',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad7]', $model->matricula_edad7, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad8</center>',
            'name' => 'matricula_edad8',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad8]', $model->matricula_edad8, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad9</center>',
            'name' => 'matricula_edad9',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad9]', $model->matricula_edad9, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad10</center>',
            'name' => 'matricula_edad10',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad10]', $model->matricula_edad10, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad11</center>',
            'name' => 'matricula_edad11',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad11]', $model->matricula_edad11, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad12</center>',
            'name' => 'matricula_edad12',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad12]', $model->matricula_edad12, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad13</center>',
            'name' => 'matricula_edad13',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad13]', $model->matricula_edad13, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad14</center>',
            'name' => 'matricula_edad14',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad14]', $model->matricula_edad14, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad15</center>',
            'name' => 'matricula_edad15',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad15]', $model->matricula_edad15, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad16</center>',
            'name' => 'matricula_edad16',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad16]', $model->matricula_edad16, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad17</center>',
            'name' => 'matricula_edad17',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad17]', $model->matricula_edad17, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad18</center>',
            'name' => 'matricula_edad18',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad18]', $model->matricula_edad18, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad19</center>',
            'name' => 'matricula_edad19',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad19]', $model->matricula_edad19, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad20</center>',
            'name' => 'matricula_edad20',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad20]', $model->matricula_edad20, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_edad21omas</center>',
            'name' => 'matricula_edad21omas',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_edad21omas]', $model->matricula_edad21omas, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_indigena</center>',
            'name' => 'matricula_indigena',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[matricula_indigena]', $model->matricula_indigena, array('title' => '',)),
        ),
        array(
            'header' => '<center>personal_plantel</center>',
            'name' => 'personal_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[personal_plantel]', $model->personal_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>personal_total</center>',
            'name' => 'personal_total',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[personal_total]', $model->personal_total, array('title' => '',)),
        ),
        array(
            'header' => '<center>personal_docente</center>',
            'name' => 'personal_docente',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[personal_docente]', $model->personal_docente, array('title' => '',)),
        ),
        array(
            'header' => '<center>personal_administrativo</center>',
            'name' => 'personal_administrativo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[personal_administrativo]', $model->personal_administrativo, array('title' => '',)),
        ),
        array(
            'header' => '<center>personal_obrero</center>',
            'name' => 'personal_obrero',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Primaria[personal_obrero]', $model->personal_obrero, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>
