<?php
/* @var $this PrimariaController */
/* @var $model Primaria */

$this->pageTitle = 'Actualización de Datos de Primaria';

      $this->breadcrumbs=array(
	'Primaria'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>