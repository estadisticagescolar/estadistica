<?php
/* @var $this PrimariaController */
/* @var $model Primaria */

$this->pageTitle = 'Registro de Primaria';

      $this->breadcrumbs=array(
	'Primaria'=>array('lista'),
	'Registro',
);
?>

<?php 
$this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); 
?>
