<?php
/* @var $this InicialController */
/* @var $model Inicial */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado'); ?>
		<?php echo $form->textField($model,'estado',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'municipio'); ?>
		<?php echo $form->textField($model,'municipio',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parroquia'); ?>
		<?php echo $form->textField($model,'parroquia',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_plantel'); ?>
		<?php echo $form->textField($model,'cod_plantel',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_estadistico'); ?>
		<?php echo $form->textField($model,'cod_estadistico', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_fundacion'); ?>
		<?php echo $form->textField($model,'fecha_fundacion',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre_plantel'); ?>
		<?php echo $form->textField($model,'nombre_plantel',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'direccion_plantel'); ?>
		<?php echo $form->textField($model,'direccion_plantel',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telefono_plantel'); ?>
		<?php echo $form->textField($model,'telefono_plantel', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datos_director'); ?>
		<?php echo $form->textField($model,'datos_director',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'correo_plantel'); ?>
		<?php echo $form->textField($model,'correo_plantel',array('size'=>40, 'maxlength'=>40, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'turno_plantel'); ?>
		<?php echo $form->textField($model,'turno_plantel',array('size'=>10, 'maxlength'=>10, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'secciones_plantel'); ?>
		<?php echo $form->textField($model,'secciones_plantel', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_totalplantel'); ?>
		<?php echo $form->textField($model,'matricula_totalplantel', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_venezolana'); ?>
		<?php echo $form->textField($model,'matricula_venezolana', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_extranjera'); ?>
		<?php echo $form->textField($model,'matricula_extranjera', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_nombregeografico'); ?>
		<?php echo $form->textField($model,'matricula_nombregeografico',array('size'=>10, 'maxlength'=>10, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_totalgeografico'); ?>
		<?php echo $form->textField($model,'matricula_totalgeografico', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_masculino'); ?>
		<?php echo $form->textField($model,'matricula_masculino', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_femenino'); ?>
		<?php echo $form->textField($model,'matricula_femenino', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_dependencianombre'); ?>
		<?php echo $form->textField($model,'matricula_dependencianombre',array('size'=>20, 'maxlength'=>20, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_dependenciaplantel'); ?>
		<?php echo $form->textField($model,'matricula_dependenciaplantel', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapamaternaltotal'); ?>
		<?php echo $form->textField($model,'matricula_etapamaternaltotal', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapamaternaledadmenosde1'); ?>
		<?php echo $form->textField($model,'matricula_etapamaternaledadmenosde1', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapamaternaledad1'); ?>
		<?php echo $form->textField($model,'matricula_etapamaternaledad1', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapamaternaledad2'); ?>
		<?php echo $form->textField($model,'matricula_etapamaternaledad2', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapapreescolartotal'); ?>
		<?php echo $form->textField($model,'matricula_etapapreescolartotal', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapapreescolaredad3'); ?>
		<?php echo $form->textField($model,'matricula_etapapreescolaredad3', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapapreescolaredad4'); ?>
		<?php echo $form->textField($model,'matricula_etapapreescolaredad4', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapapreescolaredad5'); ?>
		<?php echo $form->textField($model,'matricula_etapapreescolaredad5', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_etapapreescolaredad6'); ?>
		<?php echo $form->textField($model,'matricula_etapapreescolaredad6', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_indigena'); ?>
		<?php echo $form->textField($model,'matricula_indigena', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->