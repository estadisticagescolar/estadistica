<?php
/* @var $this InicialController */
/* @var $model Inicial */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'inicial-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-result">
                        <?php
           if($model->hasErrors()):
               $this->renderPartial('//errorSumMsg', array('model' => $model));
           else:
                ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                   endif;
               ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estado'); ?>
                                                            <?php echo $form->textField($model,'estado',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'municipio'); ?>
                                                            <?php echo $form->textField($model,'municipio',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'parroquia'); ?>
                                                            <?php echo $form->textField($model,'parroquia',array('size'=>60, 'maxlength'=>65, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'cod_plantel'); ?>
                                                            <?php echo $form->textField($model,'cod_plantel',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'cod_estadistico'); ?>
                                                            <?php echo $form->textField($model,'cod_estadistico', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_fundacion'); ?>
                                                            <?php echo $form->textField($model,'fecha_fundacion',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'nombre_plantel'); ?>
                                                            <?php echo $form->textField($model,'nombre_plantel',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'direccion_plantel'); ?>
                                                            <?php echo $form->textField($model,'direccion_plantel',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'telefono_plantel'); ?>
                                                            <?php echo $form->textField($model,'telefono_plantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'datos_director'); ?>
                                                            <?php echo $form->textField($model,'datos_director',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'correo_plantel'); ?>
                                                            <?php echo $form->textField($model,'correo_plantel',array('size'=>40, 'maxlength'=>40, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'turno_plantel'); ?>
                                                            <?php echo $form->textField($model,'turno_plantel',array('size'=>10, 'maxlength'=>10, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'secciones_plantel'); ?>
                                                            <?php echo $form->textField($model,'secciones_plantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_totalplantel'); ?>
                                                            <?php echo $form->textField($model,'matricula_totalplantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_venezolana'); ?>
                                                            <?php echo $form->textField($model,'matricula_venezolana', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_extranjera'); ?>
                                                            <?php echo $form->textField($model,'matricula_extranjera', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_nombregeografico'); ?>
                                                            <?php echo $form->textField($model,'matricula_nombregeografico',array('size'=>10, 'maxlength'=>10, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_totalgeografico'); ?>
                                                            <?php echo $form->textField($model,'matricula_totalgeografico', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_masculino'); ?>
                                                            <?php echo $form->textField($model,'matricula_masculino', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_femenino'); ?>
                                                            <?php echo $form->textField($model,'matricula_femenino', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_dependencianombre'); ?>
                                                            <?php echo $form->textField($model,'matricula_dependencianombre',array('size'=>20, 'maxlength'=>20, 'class' => 'span-12', )); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_dependenciaplantel'); ?>
                                                            <?php echo $form->textField($model,'matricula_dependenciaplantel', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapamaternaltotal'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapamaternaltotal', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapamaternaledadmenosde1'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapamaternaledadmenosde1', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapamaternaledad1'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapamaternaledad1', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapamaternaledad2'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapamaternaledad2', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapapreescolartotal'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapapreescolartotal', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapapreescolaredad3'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapapreescolaredad3', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapapreescolaredad4'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapapreescolaredad4', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapapreescolaredad5'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapapreescolaredad5', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_etapapreescolaredad6'); ?>
                                                            <?php echo $form->textField($model,'matricula_etapapreescolaredad6', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'matricula_indigena'); ?>
                                                            <?php echo $form->textField($model,'matricula_indigena', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/reportes/inicial"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <?php
            /**
             * Yii::app()->clientScript->registerScriptFile(
             *   Yii::app()->request->baseUrl . '/public/js/modules/InicialController/inicial/form.js',CClientScript::POS_END
             *);
             */
        ?>
    </div>
</div>