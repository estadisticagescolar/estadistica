<?php

/* @var $this InicialController */
/* @var $model Inicial */

$this->breadcrumbs=array(
	'Inicial'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Inicial';

?>
<div class="widget-box">
    <div class="widget-header">
        <h4>Lista de Inicial</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Inicial.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/reportes/inicial/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Inicial                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inicial-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>estado</center>',
            'name' => 'estado',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[estado]', $model->estado, array('title' => '',)),
        ),
        array(
            'header' => '<center>municipio</center>',
            'name' => 'municipio',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[municipio]', $model->municipio, array('title' => '',)),
        ),
        array(
            'header' => '<center>parroquia</center>',
            'name' => 'parroquia',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[parroquia]', $model->parroquia, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_plantel</center>',
            'name' => 'cod_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[cod_plantel]', $model->cod_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_estadistico</center>',
            'name' => 'cod_estadistico',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[cod_estadistico]', $model->cod_estadistico, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>fecha_fundacion</center>',
            'name' => 'fecha_fundacion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[fecha_fundacion]', $model->fecha_fundacion, array('title' => '',)),
        ),
        array(
            'header' => '<center>nombre_plantel</center>',
            'name' => 'nombre_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[nombre_plantel]', $model->nombre_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>direccion_plantel</center>',
            'name' => 'direccion_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[direccion_plantel]', $model->direccion_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>telefono_plantel</center>',
            'name' => 'telefono_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[telefono_plantel]', $model->telefono_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>datos_director</center>',
            'name' => 'datos_director',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[datos_director]', $model->datos_director, array('title' => '',)),
        ),
        array(
            'header' => '<center>correo_plantel</center>',
            'name' => 'correo_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[correo_plantel]', $model->correo_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>turno_plantel</center>',
            'name' => 'turno_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[turno_plantel]', $model->turno_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>secciones_plantel</center>',
            'name' => 'secciones_plantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[secciones_plantel]', $model->secciones_plantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalplantel</center>',
            'name' => 'matricula_totalplantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_totalplantel]', $model->matricula_totalplantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_venezolana</center>',
            'name' => 'matricula_venezolana',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_venezolana]', $model->matricula_venezolana, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_extranjera</center>',
            'name' => 'matricula_extranjera',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_extranjera]', $model->matricula_extranjera, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_nombregeografico</center>',
            'name' => 'matricula_nombregeografico',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_nombregeografico]', $model->matricula_nombregeografico, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_totalgeografico</center>',
            'name' => 'matricula_totalgeografico',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_totalgeografico]', $model->matricula_totalgeografico, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_masculino</center>',
            'name' => 'matricula_masculino',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_masculino]', $model->matricula_masculino, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_femenino</center>',
            'name' => 'matricula_femenino',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_femenino]', $model->matricula_femenino, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_dependencianombre</center>',
            'name' => 'matricula_dependencianombre',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_dependencianombre]', $model->matricula_dependencianombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_dependenciaplantel</center>',
            'name' => 'matricula_dependenciaplantel',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_dependenciaplantel]', $model->matricula_dependenciaplantel, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapamaternaltotal</center>',
            'name' => 'matricula_etapamaternaltotal',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapamaternaltotal]', $model->matricula_etapamaternaltotal, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapamaternaledadmenosde1</center>',
            'name' => 'matricula_etapamaternaledadmenosde1',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapamaternaledadmenosde1]', $model->matricula_etapamaternaledadmenosde1, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapamaternaledad1</center>',
            'name' => 'matricula_etapamaternaledad1',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapamaternaledad1]', $model->matricula_etapamaternaledad1, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapamaternaledad2</center>',
            'name' => 'matricula_etapamaternaledad2',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapamaternaledad2]', $model->matricula_etapamaternaledad2, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapapreescolartotal</center>',
            'name' => 'matricula_etapapreescolartotal',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapapreescolartotal]', $model->matricula_etapapreescolartotal, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapapreescolaredad3</center>',
            'name' => 'matricula_etapapreescolaredad3',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapapreescolaredad3]', $model->matricula_etapapreescolaredad3, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapapreescolaredad4</center>',
            'name' => 'matricula_etapapreescolaredad4',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapapreescolaredad4]', $model->matricula_etapapreescolaredad4, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapapreescolaredad5</center>',
            'name' => 'matricula_etapapreescolaredad5',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapapreescolaredad5]', $model->matricula_etapapreescolaredad5, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_etapapreescolaredad6</center>',
            'name' => 'matricula_etapapreescolaredad6',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_etapapreescolaredad6]', $model->matricula_etapapreescolaredad6, array('title' => '',)),
        ),
        array(
            'header' => '<center>matricula_indigena</center>',
            'name' => 'matricula_indigena',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Inicial[matricula_indigena]', $model->matricula_indigena, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>