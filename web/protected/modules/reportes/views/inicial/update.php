<?php
/* @var $this InicialController */
/* @var $model Inicial */

$this->pageTitle = 'Actualización de Datos de Inicial';

      $this->breadcrumbs=array(
	'Inicial'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>