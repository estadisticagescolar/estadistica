<?php $resultado = Inicial::model()->getPdfGeneral(); 
?>
<table style="padding: 5px; border: 5px;" width="100%" border="1" style="border-color:#F0F0F0; border-collapse:1px; font-size: 10px;">
<?php foreach ($resultado as $datos): ?>
    <tr>
        <td colspan="12" align="center" style="background:#E5E5E5; padding:5px;">
            <b> Reporte General de Educación Inicial del Plantel: <?php echo CHtml::encode($datos['nombre_plantel']); ?> </b>
        </td>
    </tr>
    <tr>
        <td><b>Estado:</b></td>
        <td ><?php echo CHtml::encode($datos['estado']); ?></td>
        
        <td><b>Municipio:</b></td>
        <td><?php echo CHtml::encode($datos['municipio']); ?></td>
    
        <td><b>Parroquia:</b></td>
        <td><?php echo CHtml::encode($datos['parroquia']); ?></td>

        <td><b>Telefono <br> del Plantel:</b></td>
        <td><?php echo CHtml::encode($datos['telefono_plantel']); ?></td> 

        <td><b>Correo Electrónico<br> del Plantel:</b></td>
        <td colspan="4"></td>

    </tr>
    <tr>
        
        <td><b>Fecha <br> Fundación:</b></td>
        <td><?php echo CHtml::encode($datos['fecha_fundacion']); ?></td>
        
        <td><b>Dirección <br> del Plantel:</b></td>
        <td><?php echo CHtml::encode($datos['direccion_plantel']); ?></td>

        <td><b>Datos<br> del Director:</b></td>
        <td></td>
        <td><b>Teléfono<br>del Director:</b></td>
        <td></td>
                  
        <td><b>Turno:</b></td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td><b>Código <br>Plantel:</b></td>
        <td colspan="4"><?php echo CHtml::encode($datos['cod_plantel']); ?>asas12213131212121asas</td>
        
        <td><b>Código<br> Estadistico:</b></td>
        <td colspan="7"><?php echo CHtml::encode($datos['cod_estadistico']); ?></td>

    </tr>
    <tr>
        <td  colspan="12"  align="center" style="background:#E5E5E5;"><b>Secciones</b></td>
    </tr>
    <tr>
        <td><b>Maternal:</b></td>
        <td colspan="4"></td>
        <td><b>Preescolar:</b></td>
        <td colspan="6"></td>
    </tr>
    <tr>
    <td  colspan="12"  align="center" style="background:#E5E5E5;"><b>Matrícula</b></td>
    </tr>
    <tr>
        <td colspan="12"><b>Total General:</b></td>
        

    </tr>
    <tr>
        <td colspan="4"><b>Nacionalidad</b></td>

        <td colspan="4"><b>Medio geográfico</b></td>

        <td colspan="4" align="center"><b>Sexo</b></td>

    </tr>
    <tr>
        <td colspan="2"><b> Venezolana:</b></td>

        <td colspan="2"><b> Extranjera:</b></td>

        <td colspan="2"><b> Urbano:</b></td>

        <td colspan="2"><b> Rural:</b></td>
        <td colspan="1"><b> Masculino:</b></td>

        <td colspan="3"><b> Femenino:</b></td>

    </tr>
    <tr>
        <td colspan="2">121312121</td>

        <td colspan="2">2121312121</td>

        <td colspan="2">12121312121</td>

        <td colspan="2">2121312121</td>

        <td colspan="1">11213121212</td>

        <td colspan="3">12121312121</td>

    </tr>
    
    <tr>
        <td colspan="12" align="center" style="background:#E5E5E5;"><b>Dependencia Oficial</b></td>
    </tr>
    <tr>
        <td colspan="2"><b>Total:</b> </td>
        <td colspan="2"><b>Nacional:</b></td>
        <td colspan="2"><b>Estadal:</b> </td>
        <td colspan="2"><b>Municipal:</b> </td>
        <td colspan="6"><b>Autónoma:</b> </td>
    </tr>
    <tr>
        <td colspan="2">2121312121</td>

        <td colspan="2">12121312121</td>

        <td colspan="2">2121312121</td>

        <td colspan="2">11213121212</td>

        <td colspan="6">12121312121</td>

    </tr>
    <tr>
        <td colspan="12" align="center" style="background:#E5E5E5;"><b>Dependencia Privada</b></td>
    </tr>
    <tr>
        <td colspan="2"><b>Total:</b></td>
        <td colspan="2"><b>Privada:</b> </td>
        <td colspan="4"><b>Privada Subvencionada MPPE:</b> </td>
        <td colspan="5"><b>Privada Subvencionada Oficial:</b> </td> 
    </tr>
    <tr>
        <td colspan="2">12121312121</td>
        
        <td colspan="2">2121312121</td>
        
        <td colspan="4">11213121212</td>
        
        <td colspan="5">12121312121</td>

    </tr>
    <tr>
        <td colspan="5" align="center" style="background:#E5E5E5;"> <b>Etapa Maternal</b></td>
        <td colspan="7" align="center" style="background:#E5E5E5;"> <b>Etapa Preescolar</b></td>
    </tr>
    <tr>
        <td  colspan="12" align="center" style="background:#E5E5E5;"><b>Edad</b></td>
    </tr>

    <tr>
        <td colspan="1"><b>Menos de 1 año</b></td>
        <td colspan="2"><b>1 año</b></td>
        <td colspan="2"><b>2 año</b></td>
        <td colspan="2"><b>3 año</b></td>
        <td colspan="2"><b>4 año</b></td>
        <td colspan="2"><b>5 año</b></td>
        <td colspan="1"><b>6 año</b></td>
        
    </tr>
<?php endforeach; ?>
</table>