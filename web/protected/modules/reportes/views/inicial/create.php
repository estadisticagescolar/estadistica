<?php
/* @var $this InicialController */
/* @var $model Inicial */

$this->pageTitle = 'Registro de Inicial';

      $this->breadcrumbs=array(
	'Inicial'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>