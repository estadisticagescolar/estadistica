<?php
/* @var $this PlantelPaeController */
/* @var $data PlantelPae */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plantel_id')); ?>:</b>
	<?php echo CHtml::encode($data->plantel_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pae_activo')); ?>:</b>
	<?php echo CHtml::encode($data->pae_activo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_inicio')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_inicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ultima_actualizacion')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ultima_actualizacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('matricula_general')); ?>:</b>
	<?php echo CHtml::encode($data->matricula_general); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('posee_simoncito')); ?>:</b>
	<?php echo CHtml::encode($data->posee_simoncito); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_ini_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_ini_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ini')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ini); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_act_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_act_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_act')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_act); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_elim')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_elim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?>:</b>
	<?php echo CHtml::encode($data->estatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_servicio_pae_id')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_servicio_pae_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('matricula_pequena')); ?>:</b>
	<?php echo CHtml::encode($data->matricula_pequena); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('matricula_mediana')); ?>:</b>
	<?php echo CHtml::encode($data->matricula_mediana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('matricula_grande')); ?>:</b>
	<?php echo CHtml::encode($data->matricula_grande); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('posee_area_cocina')); ?>:</b>
	<?php echo CHtml::encode($data->posee_area_cocina); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('condicion_servicio_id')); ?>:</b>
	<?php echo CHtml::encode($data->condicion_servicio_id); ?>
	<br />

	*/ ?>

</div>