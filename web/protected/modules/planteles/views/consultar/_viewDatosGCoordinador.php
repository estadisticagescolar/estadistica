<div class="widget-box">

    <div class="widget-header">
        <h5>Identificaci&oacute;n Del Plantel "<?php echo $model->nombre;?>"</h5>

        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div id="idenPlantel" class="widget-body" >
        <div class="widget-body-inner" >
            <div class="widget-main form">                      

                <div class="row">
                    <table>
                        <tr>
                            <td rowspan="4">
                                <p>
                                    <img class="img-thumbnail" alt="..." 
                                         <?php echo 'src="' . Yii::app()->baseUrl . '/public/images/indice.svg' . '"' ?>>
                                </p>
                            </td>
                        </tr>

                        <tr class="col-md-12">

                            <td colspan="3" style="vertical-align: top; width: 50px">
                                <?php
                                if ($model->codigo_ner != '') {
                                    ?>
                                    <label for="Plantel_cod_ner"><b>C&oacute;digo NER</b></label>
                                    <label class="span-7">
                                        <input type="text" value="<?php echo $model->codigo_ner; ?>" disabled="disbled">
                                    </label>
                                    <?php
                                }
                                ?>
                            </td>

                        </tr>

                        <tr class="col-md-12">
                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_cod_plantel" class="span-7"><b>C&oacute;digo del Plantel</b></label>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $model->cod_plantel; ?>" class="span-7" disabled="disbled">
                                </label>

                            </td>

                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_cod_estadistico" class="span-7"><b>C&oacute;digo Estad&iacute;stico</b></label>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $model->cod_estadistico; ?>" class="span-7" disabled="disbled">
                                </label>

                            </td>

                            <td style="vertical-align: top; width: 50px">

                                <label for="Pantel_denominacion_id" class="span-7"><b>Denominaci&oacute;n</b></label>
                                    
                                    <?php
                                    if(empty($model->denominacion_id))
                                    {
                                        $denominacion = '';
                                    }
                                    else
                                    {
                                        $denominacion = $model->denominacion->nombre;
                                    }
                                    ?>
                                <label class="span-7">    
                                    <input type="text" value="<?php echo $denominacion; ?>" class="span-7" disabled="disbled">
                                </label>

                            </td>
                        </tr>

                        <tr class="col-md-12">
                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_nombre" class="span-7"><b>Nombre del Plantel</b></label>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $model->nombre; ?>" class="span-7" readonly="readonly">
                                </label>
                            </td>

                            <td style="vertical-align: top; width: 50px">                                

                                <label for="Plantel_estatus_plantel_id" class="span-7"><b>Estatus</b></label>
                                    <?php
                                    if(empty($model->estatus_plantel_id))
                                    {
                                        $estatusPlantel = '';
                                    }
                                    else
                                    {
                                        $estatusPlantel = $model->estatusPlantel->nombre;
                                    }
                                    ?>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $estatusPlantel; ?>" class="span-7" disabled="disabled">
                                </label>

                            </td>

                            <td style="vertical-align: top; width: 50px">
                                
                                <label for="Plantel_annio_fundado" class="span-7"><b>A&ntilde;o de fundaci&oacute;n</b></label>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $model->annio_fundado; ?>" class="span-7" disabled="disabled">
                                </label>
                                
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<br>




<div class="widget-box collapsed" id="datosUbicacionP">

    <div class="widget-header">
        <h5>Datos de Ubicaci&oacute;n</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-down"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="display: none;">
            <div class="widget-main form">

                <div class="row">

                    <div class="col-md-4" id="divEstado">
                        
                        <label for="Plantel_estado_id" class="span-8"><b>Estado</b></label>
                            <?php
                            if(empty($model->estatus_plantel_id))
                            {
                                $estado = '';
                            }
                            else
                            {
                                $estado = $model->estado->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $estado; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divMunicipio">
                        
                        <label for="Plantel_municipio_id" class="span-8"><b>Municipio</b></label>
                            <?php
                            if(empty($model->municipio_id))
                            {
                                $municipio = '';
                            }
                            else
                            {
                                $municipio = $model->municipio->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $municipio; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divParroquia">
                        
                        <label for="Plantel_parroquia_id" class="span-8"><b>Parroquia</b></label>
                            <?php
                            if(empty($model->parroquia_id))
                            {
                                $parroquia = '';
                            }
                            else
                            {
                                $parroquia = $model->parroquia->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $parroquia; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>
                    <!--ALEXIS-->
                    <div class="col-md-4" id="divPoblacion">
                        
                        <label for="Plantel_poblacion_id" class="span-8"><b>Población</b></label>
                            <?php
                            if(empty($model->poblacion_id))
                            {
                                $poblacion = '';
                            }
                            else
                            {
                                $poblacion = $model->poblacion->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $poblacion; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divUrbanizacion">
                        
                        <label for="Plantel_urbanizacion_id" class="span-8"><b>Urbanización</b></label>
                            <?php
                            if(empty($model->urbanizacion_id))
                            {
                                $urbanizacion = '';
                            }
                            else
                            {
                                $urbanizacion = $model->urbanizacion->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $urbanizacion; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divTipoVia">
                        
                        <label for="Plantel_tipo_via_id" class="span-8"><b>Tipo de Via</b></label>
                            <?php
                            if(empty($model->tipo_via_id))
                            {
                                $tipo_via = '';
                            }
                            else
                            {
                                $tipo_via = $model->tipo_via->nb_tipo_via;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $tipo_via; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divVia">
                        
                        <label for="Plantel_via_id" class="span-8"><b>Via</b></label>
                            <?php
                            if(empty($model->via))
                            {
                                $via = '';
                            }
                            else
                            {
                                $via = $model->via;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $via; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>
                    <!--FIN-->

                    <div class="col-md-4" id="divDireccion">
                        
                        <label for="Plantel_direccion" class="span-8"><b>Direcci&oacute;n</b></label>
                        <label class="span-8">
                            <input type="text" class="span-7" value="<?php echo $model->direccion; ?>" readonly="readonly">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divTelefonoFijo">
                        
                        <label for="Plantel_telefono_fijo" class="span-8"><b>Tel&eacute;fono Fijo</b></label>
                        <label class="span-8">
                            <input type="text" value="<?php echo $model->telefono_fijo; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>
                    
                    <div class="col-md-4" id="divTelefonoOtro">
                        
                        <label for="Plantel_telefono_otro" class="span-8"><b>Otro Tel&eacute;fono</b></label>
                        <label class="span-8">
                            <input type="text" value="<?php echo $model->telefono_otro; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divNFax">
                        
                        <label for="Plantel_nfax" class="span-8"><b>Nº Fax</b></label>
                        <label class="span-8">
                            <input type="text" value="<?php echo $model->nfax; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divCorreo">
                        
                        <label for="Plantel_correo" class="span-8"><b>Correo</b></label>
                        <label class="span-8">
                            <input type="text" value="<?php echo $model->correo; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divZonaUbicacion">
                        
                        <label for="Plantel_zona_ubicacion_id" class="span-8"><b>Zona Ubicaci&oacute;n</b></label>
                            <?php
                            if(empty($model->zona_ubicacion_id))
                            {
                                $zonaUbicacion = '';
                            }
                            else
                            {
                                $zonaUbicacion = $model->zonaUbicacion->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $zonaUbicacion; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                </div> 
 

            </div>
        </div>
    </div>
</div>


<br>





<div class="widget-box collapsed" id="otrosDatosP">
    <div class="widget-header">
        <h5>Otros Datos</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-down"></i>
            </a>
        </div>
    </div>

    <div class="widget-body" id="infoGeneral">
        <div class="widget-body-inner" style="display: none;">
            <div class="widget-main form">

                <div class="row">
                    <div class="col-md-4" id="divClasePlantel">
                        
                        <label for="Plantel_clase_plantel_id" class="span-8"><b>Clase Plantel</b></label>
                            <?php
                            if(empty($model->clase_plantel_id))
                            {
                                $clasePlantel = '';
                            }
                            else
                            {
                                $clasePlantel = $model->clasePlantel->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $clasePlantel; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divCategoria">
                        
                        <label for="Plantel_categoria_id" class="span-8"><b>Categoria</b></label>
                            <?php
                            if(empty($model->categoria_id))
                            {
                                $categoria = '';
                            }
                            else
                            {
                                $categoria = $model->categoria->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $categoria; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divCondicionesEstudio">
                        
                        <label for="Plantel_condicion_estudio_id" class="span-8"><b>Condici&oacute;n Estudio</b></label>
                            <?php
                            if(empty($model->condicion_estudio_id))
                            {
                                $condicionEstudio = '';
                            }
                            else
                            {
                                $condicionEstudio = $model->condicionEstudio->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $condicionEstudio; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divGenero">
                        
                        <label for="Plantel_genero_id" class="span-8"><b>Tipo Matricula</b></label>
                            <?php
                            if(empty($model->genero_id))
                            {
                                $genero = '';
                            }
                            else
                            {
                                $genero = $model->genero->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $genero; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divTurno">
                        
                        <label for="Plantel_turno_id" class="span-8"><b>Turno</b></label>
                            <?php
                            if(empty($model->turno_id))
                            {
                                $turno = '';
                            }
                            else
                            {
                                $turno = $model->turno->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $turno; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                    <div class="col-md-4" id="divRegimen">
                        
                        <label for="Plantel_regimen_id" class="span-8"><b>Modalidad</b></label>
                            <?php
                            if(empty($model->modalidad_id))
                            {
                                $modalidad = '';
                            }
                            else
                            {
                                $modalidad = $model->modalidad->nombre;
                            }
                            ?>
                        <label class="span-8">
                            <input type="text" value="<?php echo $modalidad; ?>" class="span-7" disabled="disabled">
                        </label>
                        
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>

