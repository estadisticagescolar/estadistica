<?php
/* @var $this AulaController */
/* @var $model Ingesta */
?>
<div class="widget-box">

    <div class="widget-header">
        <h4>Ingesta</h4>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main">

                <div class="col-lg-12"><div class="space-6"></div></div>

                <a href="#" class="search-button"></a>
                <div style="display:block" class="search-form">
                    <div>


                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                            'id' => 'ingesta-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'pager' => array('pageSize' => 1),
                            'summaryText' => false,
                            'afterAjaxUpdate' => "function(){
                                
                                }",
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                            'id' => 'ingesta-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'columns' => array(
                                array(
                                    'header' => 'Tipo de Ingesta',
                                    'name' => 'tipo_ingesta_id',
                                    'value' => '$data->tipoIngesta->nombre',
                                    'filter' => CHtml::listData(
                                            TipoMenu::model()->findAll(
                                                array(
                                                    'order' => 'nombre ASC'
                                                )
                                            ), 'id', 'nombre'
                                        ),
                                ),
//                                array(
//                                    'header' => '<center title="Estatus del Aula">Estatus</center>',
//                                    'name' => 'estatus',
//                                    'filter'=> array('A' => 'Activo', 'E' => 'Inactivo'),
//                                    'value'=>array($this, 'columnaEstatus'),
//                                ),
                                array(
                                    'type' => 'raw',
                                    'header' => '<center>Acciones</center>',
                                    'filter' => CHtml::hiddenField('Aula[plantel_id]', $plantel_id, array('id' => 'Aula_plantel_id')),
                                    'value' => array($this, 'columnaAccionesAula'),
                                    'htmlOptions' => array('nowrap' => 'nowrap', 'width'=>'5%'),
                                ),
                            ),
                        ));
                        ?>




                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>

</div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>
<div id="dialogPantallaConsultar" class="hide"></div>
<div id="dialogPantallaEliminar" class="hide"> 
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas seguro de eliminar este Aula?
        </p>
    </div>
</div>
<?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/aula.js', CClientScript::POS_END);
?>
