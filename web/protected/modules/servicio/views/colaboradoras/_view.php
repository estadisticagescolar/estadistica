<?php
/* @var $this ColaboradorasController */
/* @var $data Colaborador */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen')); ?>:</b>
	<?php echo CHtml::encode($data->origen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula')); ?>:</b>
	<?php echo CHtml::encode($data->cedula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_nacimiento')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_nacimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido')); ?>:</b>
	<?php echo CHtml::encode($data->apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sexo')); ?>:</b>
	<?php echo CHtml::encode($data->sexo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_celular')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('twitter')); ?>:</b>
	<?php echo CHtml::encode($data->twitter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foto')); ?>:</b>
	<?php echo CHtml::encode($data->foto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mision_id')); ?>:</b>
	<?php echo CHtml::encode($data->mision_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('certificado_medico')); ?>:</b>
	<?php echo CHtml::encode($data->certificado_medico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manipulacion_alimentos')); ?>:</b>
	<?php echo CHtml::encode($data->manipulacion_alimentos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_id')); ?>:</b>
	<?php echo CHtml::encode($data->estado_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('municipio_id')); ?>:</b>
	<?php echo CHtml::encode($data->municipio_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parroquia_id')); ?>:</b>
	<?php echo CHtml::encode($data->parroquia_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enfermedades')); ?>:</b>
	<?php echo CHtml::encode($data->enfermedades); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alergias')); ?>:</b>
	<?php echo CHtml::encode($data->alergias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observacion')); ?>:</b>
	<?php echo CHtml::encode($data->observacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_cuenta_id')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_cuenta_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('banco_id')); ?>:</b>
	<?php echo CHtml::encode($data->banco_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero_cuenta')); ?>:</b>
	<?php echo CHtml::encode($data->numero_cuenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen_titular')); ?>:</b>
	<?php echo CHtml::encode($data->origen_titular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_titular')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_titular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_titular')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_titular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_ini_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_ini_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ini')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ini); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_act_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_act_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_act')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_act); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?>:</b>
	<?php echo CHtml::encode($data->estatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plantel_actual_id')); ?>:</b>
	<?php echo CHtml::encode($data->plantel_actual_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cant_hijos')); ?>:</b>
	<?php echo CHtml::encode($data->cant_hijos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hijo_en_plantel')); ?>:</b>
	<?php echo CHtml::encode($data->hijo_en_plantel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grado_instruccion_id')); ?>:</b>
	<?php echo CHtml::encode($data->grado_instruccion_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('busqueda')); ?>:</b>
	<?php echo CHtml::encode($data->busqueda); ?>
	<br />

	*/ ?>

</div>