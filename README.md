Estamos en construcción. Proximamente existirá una versión de prueba del sistema, apta para poder comenzar el módulo de reportes estadísticos con misma estructura del sistema actual GESCOLAR.

Se recomienda leer el contenido de la sección **WIKI** para obtener una información global de las metodologías implementadas. 
 
Existe un **backup** inicial de la Base de Datos, que incluye la estructura de los esquemas que se manejarán y los esquemas requeridos para el login de la aplicación y el registro de acceso de los usuarios.

Este backup está en **Descargas**

**Base de Datos**

*Schemas: Se trabajaran 5 esquemas principales:
1. Auditoria: incluye todas las tablas necesarias para el registro de las trazas.
2. Seguridad: incluye todas las tablas necesarias para el registro de usuarios y control de acceso.
3. Reportes: incluye todas las tablas principales de los reportes estadísticos con datos totalizados los cuales se actualizarán periódicamente con la data del Sistema de Gestión Escolar.
4. Public: incluye todas las tablas generales que son requeridas para determinar datos del usuario o son de acceso general.
5. Transacciones: incluye todas las tablas de control de actualizaciones y el registro histórico de las mismas.


*Tablas de reportes: Cada tabla creada representa un tipo de reporte estadístico, esta contendrá los campos específicos según los parámetros establecidos en la planilla por reporte correspondiente. Se manejarán los nombres de cada tabla de manera genérica, donde el reporte N°1 corresponde al nombre: reporte_1. Y estas a su vez llevarán un control en la tabla maestra reportes. 


**Instalación**

Si ya está cargado el Framework Yii en el directorio "yii" puedes ir directo al paso 4.

1.- Descargar el Framework Yii 1.*.*.

2.- Descomprimir el archivo en la raíz del proyecto.

3.- Cambiar el nombre del directorio creado después de la descompresión a "yii".

4.- Crear los directorios "web/assets", ""web/protected/runtime"" a partir de la raíz del proyecto.

5.- Crear una clase Db en el archivo "/protected/protected/configDb.php" (ver archivo "/protected/protected/configDb.php.dist") la misma debe contener lo siguiente:

    <?php

    class Db{

        public static $hostDb = 'localhost'; // Host de Base de Datos
        public static $nameDb = 'layout'; // Nombre de la Base de Datos
        public static $userDb = 'postgres'; // Usuario de la Base de Datos
        public static $passwordDb = 'postgres'; // Password de la base de datos
        public static $portDb = '5432'; // Puerto de la base de datos

        public static $hostGescolar = 'localhost'; // Host de Base de Datos
        public static $nameGescolar = 'gescolar'; // Nombre de la Base de Datos
        public static $userGescolar = 'postgres'; // Usuario de la Base de Datos
        public static $passwordGescolar = 'postgres'; // Password de la base de datos
        public static $portGescolar = '5432'; // Puerto de la base de datos

    }


6.- No debes modificar, eliminar o mover los archivos "web/protected/config/main.php", "web/protected/config/configDb.php.dist" o cualquier otro archivo común sin antes consultar al grupo.

7.- Debemos hacer lo posible para cumplir con las siguientes reglas:

  * a.- Vamos a tratar de hacer la aplicación por módulos respetando la filosofía del framework Yii.

  * b.- Los modelos serán los únicos componentes de la aplicación que permanecerán en el directorio models externo. Directorio "web/protected/models" el código del módulo tanto vistas, controladores e incluso componentes debe estar en el directorio "web/protected/modules/xxxx".

  * c.- Debemos respetar los estilos "css" de la plantilla, hacer todo para que la interfaz quede limpia.

  * d.- Las consultas SQL o ORM deben efectuarse en los modelos bajo un método que podamos consultar solo desde los controladores y luego pasemos a las vistas en forma de variable.

  * f.- Evitar en lo posible hacer consultas a la base de datos mientras se pueda, solo debemos hacer consultas cuando sea muy necesario. Si podemos guardar en variables de sesión datos de tablas catálogos hagámoslo. Siempre cuidando la filosofía del framework Yii.

  * g.- Debemos entender que hacer código de calidad y ordenado no implica perdida de tiempo sino ganancia de tiempo.

Para checkear el rol de un usuario podemos hacer uso del siguiente código en nuestros controladores:

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'ajaxList'),
                'expression' => "UserIdentity::checkAccess(array('ROLE_DEVELOPER', 'ROLE_ADMIN'))"
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
8.- Para incluir js propios debemos hacer uso del siguiente metodo (en cualquier parte de la vista, preferiblemente al final)

    <?php 
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/plantel.js',CClientScript::POS_END);
    ?>